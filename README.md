### Personal Project

2021

## L86 GNSS with ATMega328P + RFM96

- ATMega328P-AU
- RFM95/96 footprint
- L86 GNSS
- RP-SMA Antenna for RFM96

#### Schematics [https://jayanta525.gitlab.io/rfm96-l86-gnss/sch/](https://jayanta525.gitlab.io/rfm96-l86-gnss/sch/)
#### iBOM [https://jayanta525.gitlab.io/rfm96-l86-gnss/ibom/](https://jayanta525.gitlab.io/rfm96-l86-gnss/ibom/)

![img](atmega328-l86-rfm96-smd.png)
